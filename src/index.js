const express = require("express");
const http = require("http");
const path = require("path");
const socketio = require("socket.io");
const Filter = require("bad-words");
const app = express();
const server = http.createServer(app);
const io = socketio(server);
const port = process.env.PORT || 3000;
const {
  generateMessage,
  generateLocationMessage,
} = require("./utils/messages");
const {
  getUser,
  addUser,
  removeUser,
  getUsersInRoom,
} = require("./utils/users");
app.use(express.static(path.join(__dirname, "../public")));

io.on("connection", (socket) => {
  console.log("New Websocket Connection");

  //joining the Room
  socket.on("join", (options, callback) => {
    const { error, user } = addUser({ id: socket.id, ...options });

    if (error) {
      return callback(error);
    }
    socket.join(user.room);
    //when someone joined chat
    const currentUser = getUser(socket.id);
    socket.emit(
      "message",
      generateMessage("Chat-App", `Welcome ${currentUser.username}`)
    );
    //when someone joined chat to everyone expect self
    socket.broadcast
      .to(user.room)
      .emit(
        "message",
        generateMessage("Chat-App", `${user.username} has joined!`)
      );
    //to show the room data like users
    io.to(user.room).emit("roomData", {
      room: user.room,
      users: getUsersInRoom(user.room),
    });

    callback();
  });
  //sending message
  socket.on("sendMessage", (message, callback) => {
    const user = getUser(socket.id);
    const filter = new Filter();
    if (filter.isProfane(message)) {
      return callback("Profanity is not allowed");
    }
    io.to(user.room).emit("message", generateMessage(user.username, message));
    callback("Delivered");
  });

  //share location
  socket.on("sendLocation", (location, callback) => {
    const user = getUser(socket.id);
    const url = `https://google.com/maps?q=${location.lat},${location.lng}`;
    io.to(user.room).emit(
      "locationMessage",
      generateLocationMessage(user.username, url)
    );
    callback("Location shared");
  });

  //when someone leaves the chat
  socket.on("disconnect", () => {
    const user = removeUser(socket.id);
    if (user) {
      io.to(user.room).emit(
        "message",
        generateMessage(`${user.username} has left!`)
      );
      //to show the room data like users
      io.to(user.room).emit("roomData", {
        room: user.room,
        users: getUsersInRoom(user.room),
      });
    }
  });
});

server.listen(port, () => {
  console.log("App listening on port 3000!");
});
